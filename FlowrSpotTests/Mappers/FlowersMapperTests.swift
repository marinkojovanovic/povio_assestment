//
//  FlowersMapperTests.swift
//  FlowrSpotTests
//
//  Created by TK on 22/01/2018.
//  Copyright © 2018 PovioLabs. All rights reserved.
//

import XCTest
@testable import FlowrSpot

class FlowersMapperTests: XCTestCase {  
  private let flowersResponse = FlowersDataMock().mockFlowerResponses()
  
  func testMapping() {
    let entities = FlowersDataMock().mockFlowerEntities()
    XCTAssertEqual(entities.count, flowersResponse.count)
    
    for i in 0..<entities.count {
      let entity = entities[i]
      let response = flowersResponse[i]
      XCTAssertEqual(entity.name, response.name)
      XCTAssertEqual(entity.latinName, response.latinName)
      XCTAssertEqual(entity.sightings, response.sightings)
        XCTAssertEqual(entity.favorite, response.favorite)
        XCTAssertEqual(entity.profilePicture, response.profilePicture)
    }
  }
}
