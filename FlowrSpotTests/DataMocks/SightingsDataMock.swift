//
//  SightingsDataMock.swift
//  FlowrSpotTests
//
//  Created by Marinko Jovanovic on 23/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//
import Foundation
@testable import FlowrSpot

class SightingsDataMock {
    let decoder = JSONDecoder()
    let fileLoader = FileLoader()

    init() {
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }

    func mockSightingsEntities() -> [Sighting] {
        return mockSightingResponses()
    }

    func mockSightingResponses() -> [Sighting] {
        do {
            let json = try fileLoader.loadJsonFromFile("sightings")
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
            let container = try decoder.decode(SightingsContainer.self, from: data)
            return container.sightings
        } catch {
            print(error.localizedDescription)
        }
        return []
    }
}
