//
//  FlowerDetailsViewController.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import PovioKit

protocol FlowerDetailsDisplayLogic: class {
    func displayFlowerSightings(_ sightings: [Sighting])
    func displayError(_ error: RemoteResourceError)
}

class FlowerDetailsViewController: UIViewController {
    var interactor: FlowersDetailsBusinessLogic?
    var router: FlowerDetailsRoutingLogic?
    var flower: Flower?
    private lazy var contentView = FlowerDetailsContentView.autolayoutView()
    private let flowerSightingsDataSource = FlowerSightingsDataSource()

    init(flower: Flower?) {
        super.init(nibName: nil, bundle: nil)
        let interactor = FlowerDetailsInteractor()
        let presenter = FlowerDetailsPresenter()
        let router = FlowerDetailsRouter()
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        self.interactor = interactor
        self.router = router
        self.flower = flower
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        loadSightings()
    }
}

// MARK: - Display Logic
extension FlowerDetailsViewController: FlowerDetailsDisplayLogic {
    func displayFlowerSightings(_ sightings: [Sighting]) {
        flowerSightingsDataSource.update(sightings: sightings)
        contentView.collectionView.reloadData()
        contentView.emptyView.isHidden = sightings.count > 0
    }

    func displayError(_ error: RemoteResourceError) {
        router?.navigateToAlert(title: "general_error".localized(), message: error.localizedDescription, handler: nil)
        contentView.emptyView.isHidden = false
    }
}

// MARK: UICollectionView Delegate
extension FlowerDetailsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return contentView.collectionViewDimensions.sizeForItem(at: indexPath, for: collectionView)
    }
}

// MARK: - ContentView Delegate
extension FlowerDetailsViewController: AddSightingDelegate {
    func addSighting() {
        router?.navigateToAddSighting()
    }
}

// MARK: - UIScrollView Delegate
extension FlowerDetailsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = -scrollView.contentOffset.y
        let percentage = 1 - (contentOffset / contentView.headerViewHeight)
        var headerViewTranslation = -percentage * contentView.headerViewHeight

        if headerViewTranslation > 0 {
            headerViewTranslation = 0 // lock headerView
        }

        contentView.headerView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: headerViewTranslation)
    }
}

// MARK: - Private Methods
private extension FlowerDetailsViewController {
    func setupViews() {
        navigationItem.title = "general_app_name".localized()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: contentView.rightBarButton)

        setupContentView()
    }

    func setupContentView() {
        view.addSubview(contentView)
        if let flower = flower {
            contentView.headerView.setFlower(flower)
        }
        contentView.snp.makeConstraints { $0.edges.equalToSuperview() }
        contentView.collectionView.delegate = self
        contentView.collectionView.dataSource = flowerSightingsDataSource
        contentView.headerView.delegate = self
    }

    func loadSightings() {
        if let flower = flower {
            interactor?.fetchFlowerSightings(flowerId: flower.id)
        }
    }
}
