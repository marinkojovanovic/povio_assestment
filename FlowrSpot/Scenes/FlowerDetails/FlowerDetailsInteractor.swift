//
//  FlowerDetailsInteractor.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

protocol FlowersDetailsBusinessLogic {
    func fetchFlowerSightings(flowerId: Int)
}

class FlowerDetailsInteractor {
    var presenter: FlowerDetailsPresentationLogic?
    var getFlowerDetailsWorker = GetFlowerDetailsWorker()
}

// MARK: - Business Logic
extension FlowerDetailsInteractor: FlowersDetailsBusinessLogic {
    func  fetchFlowerSightings(flowerId: Int) {
        getFlowerDetailsWorker.execute(flowerId: flowerId, success: { (sightings) in
        self.presenter?.presentFlowerSightings(sightings)
      }, failure: { error in
        self.presenter?.presentFlowerSightingsError(error)
      })
    }
}
