//
//  FlowerDetailsRouter.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

protocol FlowerDetailsRoutingLogic {
    func navigateToAlert(title: String, message: String, handler: (() -> Void)?)
    func navigateToAddSighting()
}

protocol FlowerDetailsRouterDelegate: class {

}

class FlowerDetailsRouter {
    weak var viewController: FlowerDetailsViewController?
    weak var delegate: FlowerDetailsRouterDelegate?
}

// MARK: - Routing Logic
extension FlowerDetailsRouter: FlowerDetailsRoutingLogic {
    func navigateToAddSighting() {
        // TODO: Implement me
        let addSightingViewController = AddSightingViewController()
        viewController?.show(addSightingViewController, sender: nil)
    }

    func navigateToAlert(title: String, message: String, handler: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "general_ok".localized(), style: .cancel, handler: { _ in handler?()
        }))

        viewController?.present(alert, animated: true, completion: nil)
    }
}
