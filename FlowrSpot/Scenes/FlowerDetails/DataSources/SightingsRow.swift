//
//  SightingsRow.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

enum SightingsRow: RowType {
    case sighting(Sighting)
}
