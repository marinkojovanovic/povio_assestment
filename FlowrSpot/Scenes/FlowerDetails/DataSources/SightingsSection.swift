//
//  SightingsSection.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct SightingsSection: SectionType {
  let rows: [SightingsRow]
}
