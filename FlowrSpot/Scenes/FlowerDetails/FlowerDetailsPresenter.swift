//
//  FlowerDetailsPresenter.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

protocol FlowerDetailsPresentationLogic {
    func presentFlowerSightings(_ sightings: [Sighting])
    func presentFlowerSightingsError(_ error: RemoteResourceError)
}

class FlowerDetailsPresenter {
    weak var viewController: FlowerDetailsDisplayLogic?
}

// MARK: - Presentation Logic
extension FlowerDetailsPresenter: FlowerDetailsPresentationLogic {
    func presentFlowerSightingsError(_ error: RemoteResourceError) {
        viewController?.displayError(error)
    }

    func presentFlowerSightings(_ sightings: [Sighting]) {
        viewController?.displayFlowerSightings(sightings)
    }
}
