//
//  FlowerDetailsContentView.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit
import PovioKit

class FlowerDetailsContentView: UIView {
    let collectionViewDimensions = SightingCollectionViewItemDimenson(numberOfItemsInRow: 1, insets: 0)
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout()).autolayoutView()
    let headerViewHeight: CGFloat = 400
    let headerView = FlowerDetailsHeaderView.autolayoutView()
    let rightBarButton = UIButton(type: .custom)
    let emptyView = EmptyView.autolayoutView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension FlowerDetailsContentView {
    func setupViews() {
        backgroundColor = .white
        setupCollectionView()
        setupHeaderView()
        setupEmptyView()
    }

    func setupHeaderView() {
      addSubview(headerView)
      headerView.snp.makeConstraints {
        $0.leading.top.trailing.equalToSuperview()
        $0.height.equalTo(headerViewHeight)
      }
    }

    func setupEmptyView() {
      addSubview(emptyView)
      emptyView.text = "placeholder_no_content".localized()
      emptyView.snp.makeConstraints {
        $0.top.equalTo(headerView.snp.bottom)
        $0.leading.trailing.bottom.equalToSuperview()
      }
    }

    func setupCollectionView() {
      addSubview(collectionView)
      collectionView.backgroundColor = .white
      collectionView.keyboardDismissMode = .onDrag
      collectionView.contentInset = UIEdgeInsets(top: headerViewHeight + 10, left: 0, bottom: 0, right: 0)
      collectionView.register(SightingCollectionViewCell.self)
      if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
        flowLayout.scrollDirection = .vertical
        flowLayout.sectionInset = collectionViewDimensions.sectionInset
        flowLayout.minimumLineSpacing = collectionViewDimensions.lineSpacing
        flowLayout.minimumInteritemSpacing = collectionViewDimensions.interItemSpacing
      }
      collectionView.snp.makeConstraints {
        $0.edges.equalToSuperview()
      }
    }
}
