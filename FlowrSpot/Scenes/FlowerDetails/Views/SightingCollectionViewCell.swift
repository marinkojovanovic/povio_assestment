//
//  SightingCollectionViewCell.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import PovioKit
import Kingfisher

class SightingCollectionViewCell: UICollectionViewCell {
    private let flowerImageView = UIImageView.autolayoutView()
    private let userImageView = UIImageView.autolayoutView()
    private let locationLabel = UILabel.autolayoutView()
    private let userFullNameLabel = UILabel.autolayoutView()
    private let flowerNameLabel = UILabel.autolayoutView()
    private let likesButton = UIButton.autolayoutView()
    private let likesLabel = UILabel.autolayoutView()
    private let commentButton = UIButton.autolayoutView()
    private let commentsLabel = UILabel.autolayoutView()
    private let sightingsDescriptionLabel = UILabel.autolayoutView()
    private let separator = UIView.autolayoutView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public methods
extension SightingCollectionViewCell {
    func setSighting(_ sighting: Sighting) {
        flowerImageView.kf.setImage(with: URL(string: "http:\(sighting.picture)"))
        userFullNameLabel.text = "by \(sighting.user.fullName)"
        flowerNameLabel.text = "Balcony Flower"
        userImageView.image = UIImage(named: "plHero")
        sightingsDescriptionLabel.text = sighting.description
        commentButton.setImage(#imageLiteral(resourceName: "commentIcon"), for: .normal)
        commentsLabel.text = "comments".localized(sighting.commentsCount)
        likesButton.setImage(#imageLiteral(resourceName: "favoritesIcons"), for: .normal)
        likesLabel.text = "likes".localized(sighting.likesCount)
        locationLabel.text = "San Francisco, US"
    }
}

// MARK: - Private methods
private extension SightingCollectionViewCell {
    func setupViews() {
        setupFlowerImageView()
        setupUserImageView()
        setupLocationView()
        setupFlowerNameLabel()
        setupUserFullNameLabel()
        setupSightingsDescriptioLabel()
        setupSeparator()
        setupCommentsAndLikes()
    }

    func setupSeparator() {
        separator.backgroundColor = UIColor.flowrGray
        addSubview(separator)
        separator.snp.makeConstraints {
            $0.height.equalTo(0.3)
            $0.leading.equalTo(26)
            $0.centerX.equalToSuperview()
            $0.top.equalTo(sightingsDescriptionLabel.snp.bottom).offset(20)
        }
    }

    func setupCommentsAndLikes() {
        commentButton.backgroundColor = .white
        commentButton.layer.cornerRadius = 12
        commentButton.snp.makeConstraints {
          $0.size.equalTo(24)
        }

        commentsLabel.font = .custom(type: .regular, size: 12)
        commentsLabel.textColor = UIColor.flowrGray
        commentsLabel.textAlignment = .center

        let stackViewComments = UIStackView(arrangedSubviews: [commentButton, commentsLabel])
        stackViewComments.axis = .horizontal
        stackViewComments.distribution = .fillProportionally

        addSubview(stackViewComments)
        stackViewComments.snp.makeConstraints {
            $0.leading.equalTo(20)
            $0.top.equalTo(separator.snp.bottom).offset(20)
        }

        likesButton.backgroundColor = .white
        likesButton.layer.cornerRadius = 12
        likesButton.snp.makeConstraints {
          $0.size.equalTo(24)
        }

        likesLabel.font = .custom(type: .regular, size: 12)
        likesLabel.textColor = UIColor.flowrGray
        likesLabel.textAlignment = .center

        let stackViewLikes = UIStackView(arrangedSubviews: [likesButton, likesLabel])
        stackViewLikes.axis = .horizontal
        stackViewLikes.distribution = .fillProportionally

        addSubview(stackViewLikes)
        stackViewLikes.snp.makeConstraints {
            $0.leading.equalTo(stackViewComments.snp.trailing).offset(50)
            $0.top.equalTo(separator.snp.bottom).offset(20)
        }
    }

    func setupFlowerImageView() {
        addSubview(flowerImageView)
        flowerImageView.kf.indicatorType = .activity
        flowerImageView.snp.makeConstraints {
            $0.top.equalTo(self.snp.top)
            $0.trailing.equalTo(self.snp.trailing)
            $0.leading.equalTo(self.snp.leading)
        }
    }

    func setupLocationView() {
        let locationButton = UIButton.autolayoutView()
        locationButton.backgroundColor = .white
        locationButton.setImage(#imageLiteral(resourceName: "sightingListIcon"), for: .normal)

        locationLabel.font = .custom(type: .regular, size: 12)
        locationLabel.textColor = UIColor.flowrPink
        locationLabel.textAlignment = .center

        let stackViewLocation = UIStackView(arrangedSubviews: [locationButton, locationLabel])
        stackViewLocation.isLayoutMarginsRelativeArrangement = true
        stackViewLocation.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 2, leading: 10, bottom: 2, trailing: 10)
        stackViewLocation.axis = .horizontal
        stackViewLocation.distribution = .fill
        stackViewLocation.spacing = 5

        stackViewLocation.snp.makeConstraints {
          $0.height.equalTo(30)
        }

        stackViewLocation.addBackgroundAndCornerRadius(color: .white, cornerRadius: 15)

        flowerImageView.addSubview(stackViewLocation)
        stackViewLocation.snp.makeConstraints {
            $0.leading.equalTo(20)
            $0.top.equalTo(20)
        }
    }

    func setupUserImageView() {
        addSubview(userImageView)
        userImageView.layer.cornerRadius = 20
        userImageView.clipsToBounds = true
        userImageView.snp.makeConstraints {
            $0.height.equalTo(40)
            $0.width.equalTo(40)
            $0.leading.equalTo(20)
            $0.top.equalTo(flowerImageView.snp.bottom).offset(20)
        }
    }

    func setupFlowerNameLabel() {
        addSubview(flowerNameLabel)
        flowerNameLabel.font = .custom(type: .regular, size: 15)
        flowerNameLabel.textColor = UIColor.gray
        flowerNameLabel.textAlignment = .center
        flowerNameLabel.numberOfLines = 1
        flowerNameLabel.snp.makeConstraints {
            $0.leading.equalTo(userImageView.snp.trailing).offset(40)
            $0.top.equalTo(flowerImageView.snp.bottom).offset(20)
        }
    }

    func setupUserFullNameLabel() {
        addSubview(userFullNameLabel)
        userFullNameLabel.font = .custom(type: .italic, size: 12)
        userFullNameLabel.textColor = UIColor.gray
        userFullNameLabel.textAlignment = .center
        userFullNameLabel.numberOfLines = 1
        userFullNameLabel.snp.makeConstraints {
            $0.leading.equalTo(userImageView.snp.trailing).offset(40)
            $0.top.equalTo(flowerNameLabel.snp.bottom).offset(5)
        }
    }

    func setupSightingsDescriptioLabel() {
        addSubview(sightingsDescriptionLabel)
        sightingsDescriptionLabel.font = .custom(type: .regular, size: 13)
        sightingsDescriptionLabel.textColor = UIColor.gray
        sightingsDescriptionLabel.textAlignment = .center
        sightingsDescriptionLabel.numberOfLines = 1
        sightingsDescriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(27)
            $0.top.equalTo(userFullNameLabel.snp.bottom).offset(15)
        }
    }

    func setupFavoriteButton() {
        addSubview(likesButton)
        likesButton.backgroundColor = .white
        likesButton.layer.cornerRadius = 12
        likesButton.setImage(#imageLiteral(resourceName: "favoritesIcons"), for: .normal)
        likesButton.snp.makeConstraints {
            $0.top.equalTo(12)
            $0.trailing.equalTo(-12)
            $0.size.equalTo(24)
        }
    }
}
