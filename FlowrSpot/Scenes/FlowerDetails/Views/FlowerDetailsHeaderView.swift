//
//  FlowerDetailsHeaderView.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit
import SnapKit

protocol AddSightingDelegate: class {
    func addSighting()
}

class FlowerDetailsHeaderView: UIView {
    public weak var delegate: AddSightingDelegate?

    private let backgroundImageView = UIImageView.autolayoutView()
    private let favoriteButton = UIButton.autolayoutView()
    private let sightingsLabel = UILabel.autolayoutView()
    private let sightingsLabelWrapperView = UIView.autolayoutView()
    private let titleLabel = UILabel.autolayoutView()
    private let subtitleLabel = UILabel.autolayoutView()
    private let addSightingButton = UIButton.autolayoutView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public Methods
extension FlowerDetailsHeaderView {
    func setFlower(_ flower: Flower) {
        titleLabel.text = flower.name
        subtitleLabel.text = flower.latinName
        sightingsLabel.text = "sightings_count".localized(flower.sightings)
        backgroundImageView.kf.setImage(with: URL(string: "http:\(flower.profilePicture)"))
    }
}

// MARK: - Private Methods
private extension FlowerDetailsHeaderView {
    func setupViews() {
        setupBackgroundImageView()
        setupFavoriteButton()
        setupSightingsLabel()
        setupLabelWrapperView()
        setupTitleLabel()
        setupSubtitleLabel()
        setupAddSightingButton()
    }
    
    func setupBackgroundImageView() {
        addSubview(backgroundImageView)
        backgroundImageView.kf.indicatorType = .activity
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.opacity = 0.7
        backgroundImageView.layer.addSublayer(gradientLayer)
        backgroundImageView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.height.equalTo(351)
        }
    }
    
    func setupTitleLabel() {
        addSubview(titleLabel)
        titleLabel.font = .custom(type: .regular, size: 35)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 1
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(23)
            $0.top.equalTo(favoriteButton.snp.bottom).inset(-15)
        }
    }
    
    func setupSubtitleLabel() {
        addSubview(subtitleLabel)
        subtitleLabel.font = .custom(type: .regular, size: 14)
        subtitleLabel.textColor = .white
        subtitleLabel.textAlignment = .center
        subtitleLabel.alpha = 0.7
        subtitleLabel.snp.makeConstraints {
            $0.leading.equalTo(23)
            $0.top.equalTo(titleLabel.snp.bottom).inset(-15)
        }
    }
    
    func setupFavoriteButton() {
        addSubview(favoriteButton)
        favoriteButton.backgroundColor = .white
        favoriteButton.layer.cornerRadius = 15
        favoriteButton.setImage(#imageLiteral(resourceName: "favoritesIcons"), for: .normal)
        favoriteButton.snp.makeConstraints {
            $0.top.equalTo(175)
            $0.leading.equalTo(23)
            $0.size.equalTo(30)
        }
    }
    
    func setupSightingsLabel() {
        addSubview(sightingsLabel)
        sightingsLabel.font = .custom(type: .regular, size: 12)
        sightingsLabel.textColor = .white
        sightingsLabel.textAlignment = .center
        sightingsLabel.snp.makeConstraints {
            $0.top.equalTo(175)
            $0.leading.equalTo(73)
            $0.height.equalTo(30)
        }
    }
    
    func setupLabelWrapperView() {
        addSubview(sightingsLabelWrapperView)
        sightingsLabelWrapperView.backgroundColor = .black
        sightingsLabelWrapperView.alpha = 0.4
        sightingsLabelWrapperView.layer.cornerRadius = 15
        sightingsLabelWrapperView.snp.makeConstraints {
            $0.center.equalTo(sightingsLabel.snp.center)
            $0.height.equalTo(30)
            $0.width.equalTo(sightingsLabel.snp.width).offset(25)
        }
    }
    
    func setupAddSightingButton() {
        addSubview(addSightingButton)
        addSightingButton.frame = CGRect(x: 0, y: 0, width: 188, height: 48)
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = addSightingButton.bounds
        gradientLayer.colors = [UIColor(red: 236, green: 188, blue: 179, alpha: 1).cgColor, UIColor(red: 234, green: 167, blue: 158, alpha: 1).cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.cornerRadius = 3
        addSightingButton.layer.addSublayer(gradientLayer)
        
        addSightingButton.snp.makeConstraints {
            $0.height.equalTo(48)
            $0.width.equalTo(188)
            $0.leading.equalTo(23)
            $0.bottom.equalTo(backgroundImageView.snp.bottom).offset(20)
        }
        
        addSightingButton.setTitle("add_new_sighting".localized(), for: .normal)
        addSightingButton.titleLabel?.font = .custom(type: .regular, size: 14)
        addSightingButton.titleLabel?.textColor = .white
        addSightingButton.titleLabel?.textAlignment = .center
        addSightingButton.layer.cornerRadius = 3
        addSightingButton.addTarget(self, action: #selector(addSightingButtonPressed), for: .touchUpInside)
    }

    @objc func addSightingButtonPressed() {
        delegate?.addSighting()
    }
}
