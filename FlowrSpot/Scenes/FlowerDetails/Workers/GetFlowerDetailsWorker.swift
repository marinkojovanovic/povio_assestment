//
//  GetFlowerDetailsWorker.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 21/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

class GetFlowerDetailsWorker {
    var downloader = FlowersDownloader()

    func execute(flowerId: Int, success: RestClient.SuccessCompletion<[Sighting]>, failure: RestClient.FailureCompletion) {
        downloader.fetchFlowerSightings(flowerId: flowerId, success: success, failure: failure)
    }
}
