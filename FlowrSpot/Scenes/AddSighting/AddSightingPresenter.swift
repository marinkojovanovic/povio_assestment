//
//  AddSightingPresenter.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 23/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

protocol AddSightingPresentationLogic {
    func presentAddSighting(_ sighting: Sighting)
}

class AddSightingPresenter {
    weak var viewController: AddSightingDisplayLogic?
}

// MARK: - Presentation Logic
extension AddSightingPresenter: AddSightingPresentationLogic {
    func presentAddSighting(_ sighting: Sighting) {
        viewController?.displayAddSighting()
    }
}
