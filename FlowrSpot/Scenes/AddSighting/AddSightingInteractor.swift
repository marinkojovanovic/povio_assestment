//
//  AddSightingInteractor.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 23/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

protocol AddSightingBusinessLogic {
    func addSighting(sighting: Sighting)
}

class AddSightingInteractor {
    var presenter: AddSightingPresentationLogic?
}

// MARK: - Business Logic
extension AddSightingInteractor: AddSightingBusinessLogic {
    func addSighting(sighting: Sighting) {
        // TODO: Implement create sighting logic
        self.presenter?.presentAddSighting(sighting)

        // TODO: Catch Error and present
    }
}
