//
//  AddSightingView.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 23/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import PovioKit

class AddSightingView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
