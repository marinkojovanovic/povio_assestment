//
//  AddSightingViewController.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 23/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import PovioKit

protocol AddSightingDisplayLogic: class {
    func displayAddSighting()
    func diplayError(_ error: RemoteResourceError)
}

class AddSightingViewController: UIViewController {
    var interactor: AddSightingBusinessLogic?
    private lazy var contentView = AddSightingView.autolayoutView()

    init() {
        super.init(nibName: nil, bundle: nil)
        let interactor = AddSightingInteractor()
        let presenter = AddSightingPresenter()
        interactor.presenter = presenter
        self.interactor = interactor
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { $0.edges.equalToSuperview() }
    }
}
