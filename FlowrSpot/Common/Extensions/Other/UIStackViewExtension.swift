//
//  UIStackViewExtension.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 23/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

extension UIStackView {
    func addBackgroundAndCornerRadius(color: UIColor, cornerRadius: CGFloat) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.layer.cornerRadius = cornerRadius
        subView.layer.masksToBounds = true
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
