//
//  User.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 22/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct User: Codable {
    let id: Int
    let fullName: String
}
