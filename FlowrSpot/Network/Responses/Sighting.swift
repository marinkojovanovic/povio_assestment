//
//  Sighting.swift
//  FlowrSpot
//
//  Created by Marinko Jovanovic on 22/04/2020.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct Sighting: Codable {
    let id: Int
    let likesCount: Int
    let name: String
    let description: String
    let latitude: Double
    let longitude: Double
    let createdAt: String
    let picture: String
    let commentsCount: Int
    let user: User
}
